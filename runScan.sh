echo "run SCAN on our data set"
echo "run Iteration 1"
echo " data set : CA-HepTh9"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-HepTh9.txt
echo " data set : CA-HepPh12"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-HepPh12.txt
echo " data set : CA-AstroPh18"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-AstroPh18.txt
echo " data set : CA-CondMat23"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-CondMat23.txt

echo "run Iteration 2"
echo " data set : CA-HepTh9"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-HepTh9.txt
echo " data set : CA-HepPh12"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-HepPh12.txt
echo " data set : CA-AstroPh18"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-AstroPh18.txt
echo " data set : CA-CondMat23"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-CondMat23.txt

echo "run Iteration 3"
echo " data set : CA-HepTh9"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-HepTh9.txt
echo " data set : CA-HepPh12"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-HepPh12.txt
echo " data set : CA-AstroPh18"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-AstroPh18.txt
echo " data set : CA-CondMat23"
java -cp target/dscan-1.0-SNAPSHOT.jar com.dscan.SCAN CA-CondMat23.txt
