package com.dscan;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Partition {
    private int idp;
    // local edges
    private List<Edge> edges;
    // local vertices
    private List<Vertex> vertices;
    // id of vertex in this partition to verifier if one vertex is in this partition or not
    private Set<Integer> idvertices;
    // frontiers vertices with all partitions
    private List<Vertex> frantVertices;
    // cuts edges with all partitions
    private List<Edge> cutsEdges;
    // index of frontiers vertices  in others partitions
    private Set<Integer> externFrontiersVetices;
    // list of neighbors partition of the current partition
    private Set<Integer> neighborsPartition;

    public Partition() {
        // local edges
        edges = new ArrayList<Edge>();
        // local vertices
        vertices = new ArrayList<Vertex>();
        // frontiers vertices with all partitions
        frantVertices = new ArrayList<Vertex>();
        // cuts edges with all partitions
        cutsEdges = new ArrayList<Edge>();
        // list of neighbors partition of the current partition
        neighborsPartition=new HashSet<Integer>();
        // index of frontiers vertices  in others partitions
        externFrontiersVetices=new HashSet<Integer>();

        idvertices=new HashSet<Integer>();
    }
    //  function to add a vertex V to list of vertices "vertices".
    public void addVertex(Edge e,boolean border) {
        boolean tv = false;
        boolean tv2 = false;
        for (Vertex v : vertices) {
            if (v.getId() == e.getFromNode()) {
                v.addNeigbor(e.getFromNode());
                v.addNeigbor(e.getEndNode());
                tv = true;
            }
            if (v.getId() == e.getEndNode()) {
                v.addNeigbor(e.getFromNode());
                v.addNeigbor(e.getEndNode());
                tv2 = true;
            }
            // if a two nodes of the edge exist into our vertices list we exit block "for"
            if (tv && tv2) {
                break;
            }
        }
        if (tv == false) {
            Vertex vi = new Vertex(e.getFromNode());
            vi.addNeigbor(e.getFromNode());
            vi.addNeigbor(e.getEndNode());
            idvertices.add(vi.getId());
            vertices.add(vi);



            if (border)
                frantVertices.add(vi);

        }
        if (tv2 == false) {
            Vertex vi = new Vertex(e.getEndNode());
            vi.addNeigbor(e.getFromNode());
            vi.addNeigbor(e.getEndNode());


            vertices.add(vi);
            idvertices.add(vi.getId());

            if (border) {
                frantVertices.add(vi);
            }

        }
    }

    public Partition(String subGraph,int idp) {
        this.idp=idp;
        edges = new ArrayList<Edge>();
        vertices = new ArrayList<Vertex>();
        frantVertices = new ArrayList<Vertex>();
        cutsEdges = new ArrayList<Edge>();
        neighborsPartition=new HashSet<Integer>();
        externFrontiersVetices=new HashSet<Integer>();
        idvertices=new HashSet<Integer>();
        // read an input graph
        BufferedReader br = null;
        FileReader fr = null;
        try {
            fr = new FileReader(subGraph);
            br = new BufferedReader(fr);
            String line;
            boolean add_Fron_edge=false;

            while ((line = br.readLine()) != null) {

                if(!line.startsWith("#")) {
                   String parts[] = line.split("\t");


                    // if we have two nodes the line and we are still in simple adges parts
                    if (parts.length > 1 && !add_Fron_edge) {
                        int fn = Integer.parseInt(parts[0]);
                        int en = Integer.parseInt(parts[1]);
                        edges.add(new Edge(fn, en));
                      addVertex(new Edge(fn, en),false);
                    }
                    // if we have two nodes the line and we are switching to border edge "after first #"
                    else if((parts.length > 1 && add_Fron_edge))
                    {
                        int fn = Integer.parseInt(parts[0]);
                        int en = Integer.parseInt(parts[1]);
                        edges.add(new Edge(fn, en));
                        cutsEdges.add(new Edge(fn, en));
                    }
                }
                else
                {
                  add_Fron_edge=true;
                }
            }
           // get externals vertex
            for(Edge e : cutsEdges)
            {
                externFrontiersVetices.add(e.getFromNode());
                externFrontiersVetices.add(e.getEndNode());
            }
            for(Vertex v : vertices)
            {
                externFrontiersVetices.remove(v.getId());
            }

        } catch (Exception e) {
            System.out.println("Fraction ereur " + e.getMessage());
        }
    }

    public void printPartition()
    {
        System.out.println("we have "+vertices.size()+" vertices "+idvertices+" in this partition"+ this.idp);
        System.out.println("=================================================================");
        for( Vertex v : vertices)
        {
            System.out.println(v.getId()+" ");
            v.printVertex();
        }
        System.out.println("=============================");
        System.out.println("we have "+edges.size()+" edge in this partition");
        for (Edge e : edges)
        {
            System.out.println(e.getFromNode()+"    "+e.getEndNode());
        }

        System.out.println("=============================");
        System.out.println("we have "+cutsEdges.size()+" border edges in this partition");
        for (Edge e : cutsEdges)
        {
            System.out.println(e.getFromNode()+"    "+e.getEndNode());
        }
        System.out.println("we have "+externFrontiersVetices.size()+" external vertices in this partition");
        for( Integer v : externFrontiersVetices)
        {
            System.out.println(v+" ");
        }

        System.out.println("all id vertices");
        System.out.println(idvertices);
        if(this.neighborsPartition.size()>0)
        {
            System.out.println("this partition "+this.idp+" have a list of neighbors "+neighborsPartition);
        }

        System.out.println("=========================End============================");
    }

    public int getIdp() {
        return idp;
    }

    public boolean isNeighbord(Partition p){
        for(Integer i : externFrontiersVetices)
        {
            if (p.idvertices.contains(i))
            {
                return true;
            }
        }
        return false;
    }

    public void setIdp(int idp) {
        this.idp = idp;
    }

    public Set<Integer> getNeighborsPartition() {
        return neighborsPartition;
    }

    public void setNeighborsPartition(Set<Integer> neighborsPartition) {
        this.neighborsPartition = neighborsPartition;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public List<Vertex> getVertices() {
        return vertices;
    }

    public void setVertices(List<Vertex> vertices) {
        this.vertices = vertices;
    }

    public List<Vertex> getFrantVertices() {
        return frantVertices;
    }

    public void setFrantVertices(List<Vertex> frantVertices) {
        this.frantVertices = frantVertices;
    }

    public Vertex getVertex(int id)
    {
        for(Vertex v : vertices)
        {
            if (v.getId()==id)
                return  v;
        }
        return null;
    }
    // function to add externals neighbors
    public void addExternalNeighbors()
    {
        for( Edge e : cutsEdges)
        {
            if(idvertices.contains(e.getFromNode()))
            {
                getVertex(e.getFromNode()).addNeigbor(e.getEndNode());
            }
            else if(idvertices.contains(e.getEndNode()))
            {
                getVertex(e.getEndNode()).addNeigbor(e.getFromNode());
            }
        }

    }

    public List<Edge> getCutsEdges() {
        return cutsEdges;
    }

    public void setCutsEdges(List<Edge> cutsEdges) {
        this.cutsEdges = cutsEdges;
    }
    public void addPartitionNeighbor(int idp)
    {
        this.neighborsPartition.add(idp);
    }
    public void addVertexToPartition(Vertex v)
    {
        if(!vertices.contains(v.getId())) {
            vertices.add(v);
            idvertices.add(v.getId());
        }

    }
    public void addborderVertex(Vertex v)
    {
        // if a vertex V have neighbors not in the partition we must create these to ensure a correct compute of similarity
      for(Integer nei : v.getNeigbords())
      {
          addVertexToPartition(new Vertex(nei));
      }
      addVertexToPartition(v);
    }
    //several external Vertices have a neighbors not part of the partition, so we must create these
    public void addNoiseNeighbors()
    {
     for(Integer i : externFrontiersVetices)
     {
         Vertex v=getVertex(i);
         for(Integer n : v.getNeigbords())
         {
             if(!idvertices.contains(n))
             {
                 idvertices.add(n);
                 addborderVertex(new Vertex(n));
             }
         }
     }
    }
    public List<Integer>getexternalVertexOfPartion(Partition p)
    {
        List<Integer> externalvertex=new ArrayList<Integer>();
        for(Integer v : p.idvertices)
        {
            if(this.externFrontiersVetices.contains(v))
            {
                externalvertex.add(v);
            }
        }
        return externalvertex;
    }
    public void addPartitionNeighbor(Partition p)
    {
        //***************************from P in parameters => P2 to current P=> P1*********************
        //*******************************P1***********************************************************
        // in this method we test fi tow partition share at least one shared frontier Vertex
        // ad it id to the list of neighbor of the current partition
         System.out.println("cut edge for this partition in P" +this.getIdp());

        System.out.println("external vertex in P1 and from P "+p.getIdp()+" "+this.externFrontiersVetices);
        List<Integer> externalvertice=getexternalVertexOfPartion(p);
        // if we dont have any external vertices with Partition P
        if(externalvertice.size()<1)
        {
          return;
        }


        System.out.println(this.getexternalVertexOfPartion(p));
        //list vertex to be added from P1 to P2
        Set<Integer> borderVertex_p1=new HashSet<Integer>();
        // process to add all frontiers vertex from P2 to P1

        for(Integer v : externalvertice)
        {
            Vertex vx=p.getVertex(v);
            // for each cutedge we add the neighbors fo the vertex VX
            for(Edge e: this.cutsEdges)
            {
              if(e.getEndNode()==v)
              {
                  vx.addNeigbor(e.getFromNode());
                  borderVertex_p1.add(e.getFromNode());
              }
              if(e.getFromNode()==v)
              {
                    vx.addNeigbor(e.getFromNode());
                    borderVertex_p1.add(e.getFromNode());
              }
            }
            // get  all neighbors for all external vertex (vertex from P2 and has a links with the current Partition
            // all neighbors of external vertex represent a border vertex in P1
            // we must add a external  vertices in the current partition
            System.out.println("addvertextopartion  P "+this.getIdp());
            this.addVertexToPartition(vx);


            vx.printVertex();
        }
        System.out.println("we have the next vertices be migred to P"+p.getIdp()+" "+borderVertex_p1);
        // add a border vertices from P1 to P2
       System.out.println("in this step we have "+idvertices+"partition "+this.getIdp());
        // if we find one vertex not in the partition and in the list of frontier list
        // we must get it from partition P and push it to the current partition
        for(Integer i :borderVertex_p1)
        {   // if we find a vertex not part of this partition P1, we added this from P2
            // using p.getVertex(id), whene P the second partion
            if(!idvertices.contains(i))
            {   Vertex va=p.getVertex(i);
               // this.addVertexToPartition(p.getVertex(i));
                System.out.println("addbordervertice P "+this.getIdp());
                this.addborderVertex(va);
            }
        }
        //several external Vertices have a neighbors not part of the partition, so we must create these
        this.addNoiseNeighbors();
        System.out.println("we have the next vertices be migred to P"+p.getIdp()+" ( after correction)"+borderVertex_p1);

//****************************from P=>P1 in parameter to current P=>P2******************************************
        System.out.println("cut edge for  partition  P"+p.getIdp());
       System.out.println("external vertex in P2 and from P"+this.getIdp()+" "+p.getexternalVertexOfPartion(this));

        //list vertex to be added from P2 to P1
        Set<Integer> borderVertex_p2=new HashSet<Integer>();
        // process to add all frontiers vertex P1 to P2
        for(Integer v : p.getexternalVertexOfPartion(this))
        {
            Vertex vx=this.getVertex(v);
            for(Edge e: p.getCutsEdges())
            {
                if(e.getEndNode()==v)
                {
                    vx.addNeigbor(e.getFromNode());
                    borderVertex_p2.add(e.getFromNode());
                }
                if(e.getFromNode()==v)
                {
                    vx.addNeigbor(e.getEndNode());
                    borderVertex_p2.add(e.getEndNode());
                }
            }
            // get  all neighbors for all external vertex (vertex from P2 and has a links with the current Partition
            // all neighbors of external vertex represent a border vertex in P1
           // borderVertex_p1.addAll(vx.getNeigbords());
            // we must add border vertices
           // vx.printVertex();
            // add external vertex to P2
            System.out.println("addvevertextopartionP "+p.getIdp());
            p.addVertexToPartition(vx);

        }
        // process to add neighbor of frontier vertex P1 to P2
       System.out.println("we have the next vertices be migred to P2"+borderVertex_p2);
      System.out.println("in this step we have "+p.idvertices +"partition P2");
        // if we find one vertex not in the partition and in the list of frontier list
        // we must get it from partition P and push it to the current partition
        for(Integer i :borderVertex_p2)
        {
            if(!p.idvertices.contains(i))
            {   Vertex va=this.getVertex(i);
             //   va.printVertex();
               // p.addVertexToPartition(this.getVertex(i));
                System.out.println("addborderverticeP2 ");
                p.addborderVertex(va);
            }
        }
        System.out.println("we have the next vertices be migred to P1 ( after correction)"+borderVertex_p2);
        //several external Vertices have a neighbors not part of the partition, so we must create these
         p.addNoiseNeighbors();


        System.out.println("partition 1");
        for(Vertex v : vertices)
            v.printVertex();
        System.out.println("Partition 2");
        for(Vertex v : p.vertices)
            v.printVertex();


    }

    // functions for clustering

}
